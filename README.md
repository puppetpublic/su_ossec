OSSEC module
------------

The module installs OSSEC packages on Debian and Red Hat family OSes, deploys templated configuration file. The configuration options are stored in hiera and can be customized by operating system if needed. Downstream hiera overrides are also allowed to customize behavior for particular systems or groups of servers.

Default configuration of the module is taken from ISO recommendations. You probably would want to adjust it!

Note that the module enforces OSSEC installation *as local*, i.e. the logs are not being forwarded to an OSSEC server. To make use of it Splunk forwarder must be installed on the system and be configured to pick up the OSSEC logs.

## Installation

For basic use just include in your profile. 

```
# Realize the ossec users and group.
include user::ossec

# Load the su_ossec class and set the trusted sunetids.
class {'su_ossec':
  trusted_sunetids => $sunetids,
}

# Include the basic Debian ossec setup.
# Add extra collections or fragments as needed.
class { 'su_ossec::collection::debian':
  require => User['ossec'],
}

```

## Scanpaths and Ignorepaths
We use fragments manifest files to managed which files need to be scanned and which are need to be ignored.

```
manifests$ ls
collection  config_fragment.pp  fragments  init.pp  rebuild_combined_fragments.pp

fragments$ ls
bin.pp  boot.pp  etc.pp  lib.pp  opt.pp  root.pp  usr.pp  var.pp

```

We include above manifest files in manifests/collection/debian.pp

```
class su_ossec::collection::debian {

  include su_ossec

  include su_ossec::fragments::root
  include su_ossec::fragments::boot
  include su_ossec::fragments::etc
  include su_ossec::fragments::usr
  include su_ossec::fragments::bin
  include su_ossec::fragments::lib
  include su_ossec::fragments::opt
  include su_ossec::fragments::var

}

If you create new fragment file to scan/ignore files, you need to add that fragment to debian collection.

```

## Customizations

There is a number of options that can be customized, exclusions is probably the most frequently used.

#### scanpaths

An array of hashes determining which paths you want OSSEC to scan. Each has has the following keys:

- *`path`* - path to scan. _string_.
- *`report_changes`* - OSSEC supports sending diffs when changes are made to text files on Linux and unix systems. _yes/no_
- *`realtime`* - Realtime/continuous monitoring using the inotify system calls. _yes/no_

#### ignorepaths

An array of string paths to ignore, i.e. not to report on.

#### ignorepaths_regex

The same as above, but the paths are treated as regular expressions.

## Examples

Example of hiera configuration:

```YAML
ossec::scanpaths:
  - path: '/etc,/usr/bin,/usr/sbin'
    report_changes: 'no'
    realtime: 'yes'
  - path: '/bin,/sbin'
    report_changes: 'no'
    realtime: 'yes'
  - path: '/opt,/tmp,/var'
    report_changes: 'no'
    realtime: 'yes'

ossec::ignorepaths:
  - '/etc/mtab'
  - '/etc/hosts.deny'
  - '/etc/mail/statistics'
  - '/etc/random-seed'
  - '/etc/adjtime'
  - '/etc/httpd/logs'
  - '/var/mail'
  - '/var/opt/BESClient'

ossec::ignorepaths_regex:
  - '.*/cache'
```
