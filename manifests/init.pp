# Class installs ossec to work in conjuction with Splunk.
# Splunk forwarder should be configured to pick up the
# ossec logs and push them to the Splunk indexer.

class su_ossec (
  Enum['present','absent'] $ensure             = 'present',
  String                   $ossec_home         = '/var/ossec',
  Integer                  $indent             = 6,
  Enum['yes','no']         $email_notification = 'no',
  Array[String]            $trusted_sunetids   = [],
  String                   $repo_location      = 'http://debian.stanford.edu',
){
  $service_ensure = $ensure ? {
    'present' => 'running',
    default   => 'stopped',
  }

  ## Define some directories and file locations.
  $ossec_etc     = "${ossec_home}/etc"
  $conf_file     = "${ossec_etc}/ossec.conf"

  # We store all the individual fragments in $frag_dir and the
  # concatenation of all the fragment files in $combined_conf.
  $frag_dir      = "${ossec_etc}/fragments"
  $combined_conf = "${ossec_etc}/combined.conf"

  if ($::osfamily == 'Debian') {
    include apt
    $service_name = 'ossec'

    # Remove the old su_ossec repo source as the ossec package has been 
    #  deployed to the main stanford repo now.

    file { '/etc/apt/sources.list.d/ossec.list':
      ensure => 'absent',
    }

#    debian-dev was used to test the ossec package, commenting it out until
#      a possible permanent solution is decided upon for hosting this package
#    # Stanford OSSEC repository
#    apt::source { "ossec":
#      comment  => 'stanford repository for ossec packages',
#      location => "http://debian-dev.stanford.edu/debian-stanford",
#      release  => "${lsbdistcodename}",
#     repos    => 'main',
#      include  => {
#        'deb' => true,
#      },
#    }

    # Install mariadb
    case $facts['os']['distro']['codename'] {
      'stretch': {
        $mariadb_package_name = 'libmariadb2'
      }
      'buster', 'bullseye', 'bookworm', 'focal': {
        $mariadb_package_name = 'libmariadb3'
      }
      default: {
        crit("unknown os")
      }
    }

    package { $mariadb_package_name:
      ensure => 'present',
    }

    package { 'ossec-hids-server':
      ensure  => $ensure,
      require => [Class['apt'], Package[$mariadb_package_name]],
    }

  } elsif ($::osfamily == 'RedHat') {
    include yum
    $service_name = 'ossec-hids'

    package { 'ossec-hids-server':
      ensure  => $ensure,
      require => Class['yum']
    }

  } else {
    fail('OS is not supported by ossec_iso module.')
  }

  # We make some links to be more Debian-friendly.
  file {
    '/etc/ossec':
      ensure  => link,
      target  => $ossec_etc;
    '/var/log/ossec':
      ensure  => link,
      target  => "${ossec_home}/logs",
      require => Package['ossec-hids-server'];
  }

  if ($ensure == 'present') {
    file { $frag_dir:
      ensure  => directory,
      purge   => true,
      recurse => true,
      notify  => Exec['merge-template-with-combined-config'],
      require => Package['ossec-hids-server'];
    }
  } else {
    file { $frag_dir:
      ensure => absent,
      force  => true,
    }
  }

  file { "${conf_file}.template":
    ensure  => $ensure,
    content => template('su_ossec/var/ossec/etc/ossec.conf.template.erb'),
    owner   => 'root',
    group   => 'ossec',
    mode    => '0644',
    require => Package['ossec-hids-server'],
    notify  => [Service[$service_name],
                Exec['merge-template-with-combined-config']],
  }

  file { "/etc/remctl/conf.d/ossec":
    ensure  => $ensure,
    content => template('su_ossec/etc/remctl/conf.d/ossec.erb'),
    owner   => 'root',
    group   => 'ossec',
    mode    => '0644',
    require => Package['stanford-server'],
  }

  file { "/etc/cron.d/ossec-report":
    ensure  => $ensure,
    content => template('su_ossec/etc/cron.d/ossec-report.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file { "${ossec_etc}/local_internal_options.conf":
    ensure  => $ensure,
    content => template('su_ossec/var/ossec/etc/local_internal_options.conf.erb'),
    owner   => 'root',
    group   => 'ossec',
    mode    => '0644',
    require => Package['ossec-hids-server'],
    notify  => Service[$service_name],
  }

  exec { 'merge-template-with-combined-config':
    command     => 'merge-ossec-fragments',
    path        => '/usr/sbin:/usr/bin',
    refreshonly => true,
    notify      => Service[$service_name],
  }

  # Make sure ossec runs as local rather than a server
  file_line { 'ossec-run-local':
    ensure  => 'present',
    path    => '/etc/ossec-init.conf',
    match   => '^TYPE=',
    line    => 'TYPE="local"',
    require => Package['ossec-hids-server'],
    notify  => Service[$service_name],
  }

  service { $service_name:
    ensure  => $service_ensure,
    enable  => true,
    status  => 'pgrep ossec-syscheckd',
    require => Package['ossec-hids-server'],
  }

  # Ignore some innocuous messages logged by ossec.
  file { '/etc/filter-syslog/ossec':
    ensure => $ensure,
    content => template('su_ossec/etc/filter-syslog/ossec.erb'),
  }
}
