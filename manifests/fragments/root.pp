class su_ossec::fragments::root {

  $comment  = @("COMMENT"/L)
## >> START LEGACY TRIPWIRE CONFIGURATION <<
# The root home directory on most systems. Rather than individually listing
# files, we just protect the entire directory, since that way we don't have
# to worry about missing something.
#
=/                      L
/root                   R-mc    # too many things change the mod time
!/root/.aptitude/config
!/root/.bash_history
!/root/.emacs_backups
!/root/.emacs.d
!/root/.history
!/root/.history-save
!/root/.lesshst
!/root/.nano_history
!/root/.viminfo
!/root/.vim/.netrwhist
!/root/tmp

# Make sure that some critical system directories don't have anything
# weird happen to them.
=/proc                  L-n     # processes change link count
=/tmp                   L-n     # subdirectories change link count
## >> END LEGACY TRIPWIRE CONFIGURATION <<
#
# We ignore access credentials used by some processes
# to upload data to Google Cloud Storage as these credentials
# change fairly often.
#
# The /root/.mysql_history is like a bash history file for MySQL: it
# stores a history of MySQL commands run from the MySQL prompt. It can
# can safely be ignored.
#
# The /root/.rnd file appears to arise when openssl is generating randomness
# and needs a seed file.
#
# The .wget-hsts file is how wget tracks HSTS. We can safely ignore it.
| - COMMENT

  su_ossec::config_fragment { 'root':
    ensure => 'present',
    comment => $comment,
    ignores => [
      '/root/.aptitude/config',
      '/root/.bash_history',
      '/root/.emacs_backups',
      '/root/.emacs.d',
      '/root/.history',
      '/root/.history-save',
      '/root/.lesshst',
      '/root/.mysql_history',
      '/root/.nano_history',
      '/root/.viminfo',
      '/root/.rnd',
      '/root/.rpmdb/',
      '/root/.vim/.netrwhist',
      '/root/.wget-hsts',
      '/root/tmp',
      '/root/.config/gcloud/credentials.db',
      '/root/.config/gcloud/logs/',
      '/root/.gsutil/credstore2',
      '/root/.config/gcloud/access_tokens.db',
    ],
    scan_dirs => [
      '/root',
    ],
    log_dirs => [
      '/',
      '/tmp',
      '/proc',
    ],
  }
}
