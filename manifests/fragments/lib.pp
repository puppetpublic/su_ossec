class su_ossec::fragments::lib {

  $comment  = @("COMMENT"/L)
## >> START LEGACY TRIPWIRE CONFIGURATION <<
# Critical system binaries and binaries likely to be run by root
/lib                    R

# Don't check timestamp changes on modules, since the dependency files are
# rebuilt on boot.
/lib/modules            R-mc
## >> END LEGACY TRIPWIRE CONFIGURATION <<
#
# Given ossec simpler scanning features we consolidate /lib/modules into
# /lib.
# Ignore /lib/google-cloud-sdk.
| - COMMENT

  su_ossec::config_fragment { 'lib':
    ensure => 'present',
    comment => $comment,
    ignores => [
      '/lib/google-cloud-sdk',
    ],
    scan_dirs => [
      '/lib',
    ],
    log_dirs => [],
  }
}
