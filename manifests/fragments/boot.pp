class su_ossec::fragments::boot {

  $comment  = @("COMMENT"/L)
# The kernel itself and related files.  Red Hat fiddles with System.map and
# the /boot directory itself.
| - COMMENT

  su_ossec::config_fragment { 'boot':
    ensure => 'present',
    comment => $comment,
    ignores => [],
    scan_dirs => [
      '/boot',
    ],
    log_dirs => [],
  }
}
