class su_ossec::fragments::usr {

  $comment  = @("COMMENT"/L)
## >> START LEGACY TRIPWIRE CONFIGURATION <<
# The following files are setuid or setgid (or may be in the default
# configuration), and therefore deserve special consideration.  No need to
# put anything in /bin here, since that's already checked at R.
/usr/bin/crontab        R
/usr/bin/ksu            R
/usr/bin/mailx          R
/usr/bin/newgrp         R
/usr/bin/uptime         R
/usr/bin/w              R
/usr/bin/write          R
/usr/sbin               R

# Apparently changes nightly on systems that are running Tivoli with
# encrypted backups, or possibly just the 5.5 client.
!/usr/lib/tivoli/tsm/client/ba/bin/tsmstats.ini

# Less critical files.  In directories containing files that could be
# modified, we save checksums.  See below for a list of specific
# exceptions (such as suid/sgid binaries) for which we do more checking.
/usr/bin                R-2
/usr/lib                R-2
/usr/local              R-2
## >> END LEGACY TRIPWIRE CONFIGURATION <<
#
# Given ossec simpler scanning we can consolidate all the /usr/bin/ programs
# (e.g., /usr/bin/crontab) in the first section into just scanning /usr/bin.
#
# We don't use tivoli so we don't bother to ignore
# /usr/lib/tivoli/tsm/client/ba/bin/tsmstats.ini.

# Ignore:
# google-cloud-sdk - This package updates frequently and we want to ignore it.
#
# /usr/share/spdb/tmp: Rails likes to put its temporary files in a local tmp/
# directory. SPDB is a Rails application so we need to ignore that directory.
| - COMMENT

  su_ossec::config_fragment { 'usr':
    ensure => 'present',
    comment => $comment,
    ignores => [
      '/usr/lib/google-cloud-sdk',
      '/usr/share/spdb/tmp',
    ],
    scan_dirs => [
      '/usr/bin',
      '/usr/lib',
      '/usr/local',
      '/usr/sbin',
      '/usr/share',
    ],
    log_dirs => [
    ],
  }
}
