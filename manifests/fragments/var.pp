class su_ossec::fragments::var {

  $comment  = @("COMMENT"/L)
## >> START LEGACY TRIPWIRE CONFIGURATION <<
# Ignore everything in /var.  If packages put their changing data in /var
# the way that they're supposed to, this means we need very little change
# or customization for individual services.  Do watch crontabs as an
# exception.
=/var                   L-n
/var/spool/cron         R
## >> END LEGACY TRIPWIRE CONFIGURATION <<
| - COMMENT

  su_ossec::config_fragment { 'var':
    ensure => 'present',
    comment => $comment,
    ignores => [
      '/var/log/OLD',
      '/var/lib/ldap/logs',
      '/var/ossec/stats',
      '/var/ossec/logs',
    ],
    scan_dirs => [
      '/var/spool/cron',
    ],
    log_dirs => [
      '/var',
    ],
  }
}
