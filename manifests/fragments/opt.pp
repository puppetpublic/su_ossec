class su_ossec::fragments::opt {

  $comment  = @("COMMENT"/L)
## >> START LEGACY TRIPWIRE CONFIGURATION <<
#Dell OpenManage maintains its own XML logs, which aren't of interest.
!/opt/dell/srvadmin/var/log/openmanage/omcmdlog.xml
!/opt/dell/srvadmin/var/log/openmanage/PRtaskreport.xml
!/opt/dell/srvadmin/var/log/openmanage/dcsys64.xml

# Less critical files.  In directories containing files that could be
# modified, we save checksums.  See below for a list of specific
# exceptions (such as suid/sgid binaries) for which we do more checking.
/opt
## >> END LEGACY TRIPWIRE CONFIGURATION <<
# We don't need to worry about the Dell stuff anymore, so we don't bother
# to ignore.
#
# /opt/puppetlabs/puppet/cache/: This contains pupet run reports and
# cached Puppet modules, so is safe to ignore.
| - COMMENT

  su_ossec::config_fragment { 'opt':
    ensure => 'present',
    comment => $comment,
    ignores => [
      '/opt/puppetlabs/puppet/cache/',
      '/opt/dell/',
      '/opt/splunkforwarder/var/log/',
      '/opt/splunkforwarder/var/lib/',
    ],
    scan_dirs => [
      '/opt',
    ],
    log_dirs => [],
  }
}
