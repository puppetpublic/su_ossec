class su_ossec::fragments::bin {

  $comment  = @("COMMENT"/L)
# Critical system binaries and binaries likely to be run by root
/bin                    R
/sbin                   R
| - COMMENT

  su_ossec::config_fragment { 'bin':
    ensure => 'present',
    comment => $comment,
    ignores => [
    ],
    scan_dirs => [
      '/bin',
      '/sbin',
    ],
    log_dirs => [
    ],
  }
}
