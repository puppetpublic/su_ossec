class su_ossec::fragments::etc {

  $comment  = @("COMMENT"/L)
## >> START LEGACY TRIPWIRE CONFIGURATION <<
# Configuration files.  There's a lot of pruning here, since many of these
# files change routinely during regular operation of Linux.  We default
# to checking everything and then prune out what we don't want to check or
# change it to L if we don't want to watch for changes.
/etc                    R-mc    # /etc itself changes on boot
!/etc/adjtime                   # modified on boot, may not be present
!/etc/keytabs                   # rekeyed periodically
/etc/krb5.keytab        L       # rekeyed periodically
!/etc/lbcd.pid                  # recreated on boot
!/etc/mail/statistics           # changes when mail is sent out (sendmail 8.10)
!/etc/motd              L       # changes are almost certainly wanted
!/etc/mtab                      # recreated on boot
!/etc/ntp.drift                 # older Debian still has this
/etc/shadow             L       # users changing their passwords
!/etc/webauth/keytab            # rekeyed periodically

# Changes too frequently and isn't security-sensitive enough to warrant
# monitoring.
!/etc/filter-syslog

# Updated automatically by cron by the help desk's changes to a
# workgroup.
!/etc/remctl/acl/helpdesk-all

# BigFix Client
!/etc/opt/BESClient
## >> END LEGACY TRIPWIRE CONFIGURATION <<
#
# The file /etc/krb5.keytab is rekeyed every so often, so we
# can ignore changes to it.
| - COMMENT

  su_ossec::config_fragment { 'etc':
    ensure => 'present',
    comment => $comment,
    ignores => [
      '/etc/adjtime',
      '/etc/keytabs',
      '/etc/krb5.keytab',
      '/etc/lbcd.pid',
      '/etc/mail/statistics',
      '/etc/webauth/keytab',
      '/etc/motd',
      '/etc/mtab',
      '/etc/ntp.drift',
      '/etc/filter-syslog',
      '/etc/opt/BESClient',
      '/etc/puppetlabs/mcollective',
      '/etc/remctl/acl/helpdesk-all',
      '/etc/remctl/acl/as-cia',
      '/etc/remctl/acl/as-cia-root',
      '/etc/remctl/acl/coreinfra',
      '/etc/remctl/acl/coreinfra-root',
      '/etc/remctl/acl/helpdesk-all',
      '/etc/remctl/acl/its-idg-windows',
      '/etc/remctl/acl/its-idg-windows-root',
      '/etc/remctl/acl/its-sa-app-root',
      '/etc/remctl/acl/its-sa-app',
      '/etc/remctl/acl/its-sa-core',
      '/etc/remctl/acl/its-sa-core-root',
      '/etc/remctl/acl/its-sa-crc',
      '/etc/remctl/acl/its-sa-crc-root',
    ],
    scan_dirs => [
      '/etc',
    ],
    log_dirs => [
      '/etc/shadow',
      '/etc/motd',
    ],
  }
}
