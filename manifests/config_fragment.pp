#
# $log_dirs: each element of this array is treated
# as directory which should NOT be recursed into but the
# directory's permissions should be monitored.
# Example: $log_dirs => ['/var/log']
#
define su_ossec::config_fragment (
  Enum['present', 'absent'] $ensure = undef,
  String                    $extension = 'frag',
  #
  String                    $comment   = undef,
  Array[String]             $ignores   = undef,
  Array[String]             $scan_dirs = undef,
  Array[String]             $log_dirs  = undef,
){

  include su_ossec
  $frag_dir  = $su_ossec::frag_dir

  file { "${frag_dir}/${name}.${extension}":
    ensure  => $ensure,
    content => template('su_ossec/config-fragment.erb'),
    notify  => Exec['merge-template-with-combined-config'],
    require => File[$frag_dir],
  }

}
