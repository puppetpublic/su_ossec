class su_ossec::collection::debian {

  include su_ossec

  include su_ossec::fragments::root
  include su_ossec::fragments::boot
  include su_ossec::fragments::etc
  include su_ossec::fragments::usr
  include su_ossec::fragments::bin
  include su_ossec::fragments::lib
  include su_ossec::fragments::opt
  include su_ossec::fragments::var

}
