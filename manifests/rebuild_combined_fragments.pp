class su_ossec::rebuild_combined_fragments {

  include su_ossec
  $conf_file     = $su_ossec::conf_file
  $combined_conf = $su_ossec::combined_conf
  $frag_dir      = $su_ossec::frag_dir

  $cmd = "cat $frag_dir/* > $combined_conf"
  exec { 'rebuild-combined-fragments':
    command     => $cmd,
    refresh     => $cmd,
    refreshonly => true,
    path        => '/bin',
    require     => File[$frag_dir],
    before      => File[$conf_file],
  }

  # It is possible for the $combined_conf file to NOT exist and the above
  # exec to not be triggered.  For this edge case we have this exec.
  exec { 'rebuild-combined-fragments-if-not-exists':
    command     => $cmd,
    creates     => $combined_conf,
    path        => '/bin',
    require     => File[$frag_dir],
    before      => File[$conf_file],
  }


}
